﻿If you have questions, please open an issue and let me know. I will do what I can to help.

This is an example [Grafana](https://grafana.com/) dashboard for the [Robinhood Policy Engine](https://github.com/cea-hpc/robinhood). The purpose behind its creation was to satisfy the need of a single dashboard from multiple Robinhood instances. The Robinhood web-gui is extremely full featured, but limited to one instance at a time. This proved to be troublesome for non-privileged account users needing the information Robinhood provides.

These example JSON codes were licensed by my current employer. Please read the license for the exact legal wording. The premise of the license is this, the code is free to use, modify, distribute, ect. If redistributed, keep the license. If modified, the licensor just wants author credit. In both cases the licensor is not responsible for problems, and should not be cited as endorsing any of the projects. “Use of” is not “endorsement of”. If you have questions or need this relicensed, please contact me and I will put you in touch with the people who can make those decisions.

How to use this JSON code.

1. Proper installation and configuration of Robinhood should be completed. Each file system that needs to be scanned should be configured to a separate database. This example uses the MySQL databases robinhood_home, robinhood_data, and robinhood_archive.

2. Grafana should be installed. It can be installed on the same host as the Robinhood webgui as it uses non-conflicting apache settings.

3. Grafana needs the “Pie” plugin which can be installed via the Grafana plugin options.

4. Grafana should be configured to talk to the MySQL instance. This can be done under the Configuration->Data Sources. Using robinhood_home as an example, it is configured as the data source “home”. Best practice is that the Grafana user has read only access to each database. For the purpose of simple example, configuration using the same Robinhood user/pass that was used for the scan can (but REALLY shouldn’t) be used. Proper configuration of MySQL is left to the admin.

5. From Grafana’s Create (the + sign)-> Import copy and paste the JSON into the paste field and click next. The page should show a name that can be changed, a Folder to place it in (General should be default), a Unique Identifier (if it complains, click the blue ‘change’ button until it is happy or just create a new id), and a data source(s) which should match the input.

There are two JSON files.

The first is labeled “home-example.json” but this template is generic enough that it can be used for all three data sources in the example above. Just change the name to match when doing the import. This Dashboard should show at the top the last time the scan was run on the data source. Under that the Top Users disk usage as a donut chart with the user id and total amount of space underneath. Beside it is the Top Groups disk usage with the same metrics. Finally on that row is the Top Users in terms of number of files on the disk. Below that will be the file histogram matching the same output that Robinhood stores in its database. Below that is the file sizes report which should be fully sortable based on each metric. Lastly, the file age by year breakdown with the legend showing year and number of files.

The second is topusage.json. This is a sample that will show when the last time each of the file systems were scanned at the top. Then it shows how to blend reports from each of the three sources. The first donut chart shows the top users in disk usage from all three file systems as opposed to the second donut which only shows the top users in disk usage from the home file system.

The blended donut chart is most likely to be the one that “breaks”. If it does not display correctly it is probably because the database values don’t align properly. If you click the downarrow when hovering over the chart title, then click “Edit” it will allow you to tweak the SQL code. Most likely you will need to change the “robinhood_data” in the “FROM robinhood_data.ACCT_STAT” to the database name for your data. Do the same for home and archive.

I hope that this simple example gives you an idea of how to extend the functionality of Robinhood with all of the file system databases that Robinhood scans for you. These are just a few examples of how we’ve extended it. Most of the other changes we’ve made are either derivative of these examples or so specific to our needs that they would not be useful.

The only TODO item is to figure out how to get this into the [Grafana Dashboards community](https://grafana.com/dashboards) for easier access by the community.

Happy computing!

This dashboard example was put together by Lyle Vanfossan and Chris Stackpole.
